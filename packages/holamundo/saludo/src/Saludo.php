<?php

namespace HolaMundo\Saludo;

class Saludo
{
    public function saludar(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
